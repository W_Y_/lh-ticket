var ApiUrl = {
	'testHost': 'http://crm.locationhunter.net/' 	
};

var host = ApiUrl.testHost;

var ApiServer = {
	_setAuthOrization : function () {
		var user = JSON.parse(sessionStorage.getItem('user'))?JSON.parse(sessionStorage.getItem('user')): false;
		// console.log(user)
		if (!user) {
			alert('登录信息未通过');
			window.location.href = '/ticket/index.html';
			return;
		}

		return {
			'AuthOrization': 'apikey ' + user.username + ':' + user.apikey,
			'Content-Type' : 'application/json; charset=UTF-8'
		}
	},

	_sendMessage : function (phone) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/member/send_code/?phone=' + phone + '&action=signin'
		});
	},

	_buyTicketLogin : function (phone, code) {
		var params = {
			'phone': phone,
			'code': code
		};

		return $.ajax({
			method: 'POST',
			url: host + 'api/v1/member/login/',
			data: JSON.stringify(params),
			headers: {
				'Content-Type' : 'application/json; charset=UTF-8'
			}
		});
	},

	_getUserInf : function (id) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/member/' + id + '/',
			headers: ApiServer._setAuthOrization()
		});
	},

	_getExhibitionList : function () {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/exhibition/'
		})
	},

	_getExhibitionDetail : function (exhibitionId) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/exhibition/' + exhibitionId + '/'
		})
	},

	_getExhibitionTicketList : function (exhibitionId) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/ticket/?exhibition=' + exhibitionId,
			headers: ApiServer._setAuthOrization()
		})
	},

	//order
	_createOrder: function (exhibitionId, orderItems) {
		var params = {
			'exhibition': exhibitionId,
			'items': orderItems // [{'ticket': ticketId, 'count': 1}]
		}
		console.log(params);
		 
		return $.ajax({
			method: 'POST',
			url: host + 'api/v1/order/',
			data: JSON.stringify(params),
			headers: ApiServer._setAuthOrization()
		})
	},

	_getOrder : function (status) {
		var rUrl = 'api/v1/order/?offset=0&limit=1000&order_by=-created_at';
		if (status) {
			rUrl = rUrl + '&status=' + status;
		}

		return $.ajax({
			method: 'GET',
			url: host + rUrl,
			headers: ApiServer._setAuthOrization()
		})
	},

	_getOrderDetail : function (orderId) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/order/' + orderId + '/',
			headers: ApiServer._setAuthOrization()
		})
	},

	_getWechatPaymentConfig : function (orderId, openId) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/order/' + orderId + '/pay/?openid=' + openId,
			headers: ApiServer._setAuthOrization()
		})   
    },

    
	_getCountData :function () {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/exchange/statistic/',
			headers: ApiServerInternal._setAuthOrization()
		})
	},

};

var ApiServerInternal = {
	_setAuthOrization : function () {
		var user = JSON.parse(localStorage.getItem('internalUser'))?JSON.parse(localStorage.getItem('internalUser')): false;
		// console.log(user)
		if (!user) {
			alert('登录信息未通过');
			window.location.href = '/ticket/sections/insider/insider.html';
			return;
		}

		return {
			'AuthOrization': 'apikey ' + user.username + ':' + user.apikey  
		}
	},

	_getOrderDetailWx :function (url) {
		return $.ajax({
			method: 'GET',
			url: url,
			headers: ApiServerInternal._setAuthOrization()
		})
	},

	_getOrderDetailOrderNo :function (order_no) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/order/?order_no=' + order_no ,
			headers: ApiServerInternal._setAuthOrization()
		})
	},

	_getOrderDetailPhone :function (phone) {
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/order/?phone=' + phone + '&limit=1000',
			headers: ApiServerInternal._setAuthOrization()
		})
	},

	_exchangeTicket: function (orderId, idList, type) {
		var item = '';

		idList.forEach(function (elem, index) {
			// console.log(elem)
			if (index == 0) {
				item = elem.object_id;
			} else {
				item = elem.object_id + ',' + item;
			}
		});
		return $.ajax({
			method: 'GET',
			url: host + 'api/v1/exchange/?order=' + orderId + '&type=' + type +'&items=' + item,
			headers: ApiServerInternal._setAuthOrization()
		})
	},

}