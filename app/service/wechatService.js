  var wechat = {};
      wechat.testHost = 'http://crm.locationhunter.net/';
      wechat.host = wechat.testHost;

  var WechatJsSdk = {
      _getConfigParams : function (url) {
        return $.ajax({
            method: 'GET',
            url: wechat.host + 'crm-wechat/signature/?url=' + url
        })
      },

      _readyConfig : function (json) {
        wx.config({
          debug: false,
          appId: json.appId,
          timestamp: json.timeStamp,
          nonceStr: json.nonceStr,
          signature: json.signature,
          jsApiList: [
            'scanQRCode',
          ]
        });
      },
  };

    
 