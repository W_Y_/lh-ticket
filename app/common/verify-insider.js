var VerifyInsider = {
	_verifyLogin : function () {
		var loginData = localStorage.getItem('internalUser')?localStorage.getItem('internalUser'): false;
		if (loginData != '' && loginData != 'undefined' && loginData.apikey != '' && loginData.apikey != 'undefined') {
			return;
		} else {
			alert('登录信息失效，请重新登录');
			window.location.href = "/ticket/sections/insider/insider.html";
			return;
		};
	},
}

VerifyInsider._verifyLogin();