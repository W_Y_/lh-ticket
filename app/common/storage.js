$(document).ready(function() {});

var Storage = {
  setSessionStorage: function(name, value) {
    sessionStorage.setItem(name, JSON.stringify(value));
  },

  setLocalStorage: function(name, value) {
    localStorage.setItem(name, JSON.stringify(value));
  },

  getSessionStorage: function(name) {
    return JSON.parse(sessionStorage.getItem(name));
  },

  getLocalStorage: function(name) {
    return JSON.parse(localStorage.getItem(name));
  },

  removeSessionStorage: function(name) {
    sessionStorage.removeItem(name);
  },

  removeLocalStorage: function(name) {
    localStorage.removeItem(name);
  }
};

var SetExInf = {
  _checkExhibitionInformation: function(data) {
    SetExInf._setCoverImg(data);
    SetExInf._setTitle(data);
  },

  _setCoverImg: function(data) {
    $("header").css({
      background: " url('" + data.cover_image + "')",
      "background-size": "cover",
      "background-repeat": "no-repeat",
      "background-position": "center center"
    });
  },

  _setTitle: function(data) {
    document.title = "首页 | " + data.title;
  }
};
