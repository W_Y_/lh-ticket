 //设置html font-size
$(document).ready(function () {
    Common._setFontSize();
    // Common._judgeOpenid();
})

var Common = {
    _setFontSize: function () {
         var docEl = document.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = function () {
                var clientWidth = docEl.clientWidth;
                if (!clientWidth) {
                    return;
                } else if (clientWidth > 640) {
                    docEl.style.fontSize = 20 + 'px';
                } else {
                    docEl.style.fontSize = 20 * (clientWidth / 640) + 'px';
                }
            };

        if (!document.addEventListener) return;
        window.addEventListener(resizeEvt, recalc, false);
        recalc();
    },

    _judgeOpenid : function () {
        var whiteList = ['insider', 'count', 'exchange-ticket', 'verify'];
        var visitUrl = window.location.href;
        var isVisitWhite = false;
        whiteList.forEach(function (elem, item) {
            if (visitUrl.indexOf(elem) != -1) {
                isVisitWhite = true;
                return;
            }
        })

        if (isVisitWhite) {//设置不需要判断openid的白名单
            return;
        }

        var opid;
        
        opid = Common._getUrlParams()['openid'] ? Common._getUrlParams()['openid'] : false;
        
        if (!opid) {
            opid = sessionStorage.getItem('user') ? JSON.parse(sessionStorage.getItem('user'))['openid']: false;
            if (!opid) {
                window.location.href = "http://crm.locationhunter.net/tjump/";
            }   
        } 
    },

    _getUrlParams: function (searchUrl) {  
       var url = searchUrl ? searchUrl : location.search;
       var UrlParams = new Object();  
       if (url.indexOf("?") != -1) {  
          var str = url.substr(1);  
          strs = str.split("&");  
          for(var i = 0; i < strs.length; i ++) {  
             UrlParams[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);  
          }  
       }  
       return UrlParams;  
    }, 

    getNowFormatDate:function (time) {
        var date = new Date(time * 1000);
        var seperator1 = ".";
        var seperator2 = ":";
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        var strHours = date.getHours();
        var strMinutes = date.getMinutes();
        var strSecond = date.getSeconds();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }

        if (strHours >= 0 && strHours <= 9) {
            strHours = "0" + strHours;
        }
        if (strMinutes >= 0 && strMinutes <= 9) {
            strMinutes = "0" + strMinutes;
        }
        if (strSecond >= 0 && strSecond <= 9) {
            strSecond = "0" + strSecond;
        }
        var currentdate = {
            'date' : date.getFullYear() + seperator1 + month + seperator1 + strDate ,
            'time' : strHours + seperator2 + strMinutes 
        };
        // console.log(currentdate)
        return currentdate;
    },
};