$(document).ready(function () {
	Index._watchPhoneNumber();
	Index._watchCodeNumer();

	$('#code').val(''); // default code value
	$('#phone').val(''); // default phone value

	$('.code').on('click', '.get-code', function () {
		Index._sendCode();
	});

	$('.code i').on('click', function () {
		$('#code').val('');
		$('.code i').css('display', 'none');
		VerifyParams.code = '';
		Index._startVerify();
	});

	$('footer').on('click', '.send-verify', function () {
		Index._sendVerify();
	});

});

var VerifyParams = {
	'phone': '',
	'code': ''
};

var Status = {
	'countdown': 'false',
};


var Index = {

	_watchPhoneNumber: function () {
		var reg = /^1[0-9]{10}$/;
		var flag;
		$('#phone').on('keyup', function () {
			var numValue = $('#phone').val();
			flag = reg.test(numValue);
			if (numValue.length == 11 && Status.countdown == 'false' && flag == true) {
				VerifyParams.phone = numValue;
				// console.log(VerifyParams);
				$('.code span').removeClass('disabled-bgc');
				$('.code span').addClass('submit-bgc');
				$('.code span').addClass('get-code');
			} else {
				VerifyParams.phone = '';
				$('.code span').removeClass('get-code');
				$('.code span').removeClass('submit-bgc');
				$('.code span').addClass('disabled-bgc');
			}
		})

	},

	_watchCodeNumer: function () {
		$('#code').on('keyup', function () {
			var codeValue = $('#code').val();
			if (codeValue.length > 0) {
				$('.code i').css('display', 'inline-block');
				VerifyParams.code = codeValue;

				Index._startVerify();
			} else {
				VerifyParams.code = codeValue;
				$('.code i').css('display', 'none');
				Index._startVerify();
			}
		})
	},

	_sendCode: function () {
		ApiServer._sendMessage(VerifyParams.phone).done(function (json) {
			// console.log(json);
			Index._countDown();
		});
	},

	_countDown: function () {
		Status.countdown = 'true';
		var time = 60;
		$('.code span').removeClass('get-code');
		$('.code span').removeClass('submit-bgc');
		$('.code span').addClass('disabled-bgc');

		var countdown = setInterval(function () {
			time = time - 1;
			$('.code span').text(time + 's');
		}, 1000);

		setTimeout(function () {
			clearInterval(countdown);
			Status.countdown = 'false';
			$('.code span').text('获取');

			$('.code span').removeClass('disabled-bgc');
			$('.code span').addClass('submit-bgc');
			$('.code span').addClass('get-code');
		}, 60 * 1000)
	},

	_startVerify: function () {
		var result;

		if (VerifyParams.phone.length == 11 && VerifyParams.code.length > 0) {
			result = 'true';
		} else {
			result = 'false';
		}
		// console.log(result)
		if (result == 'true') {
			$('.verify').removeClass('disabled-bgc');
			$('.verify').addClass('submit-bgc');
			$('.verify').addClass('send-verify');

			$('.order').addClass('orange');
			$('.order').addClass('view-order');
		} else {
			$('.verify').removeClass('submit-bgc');
			$('.verify').addClass('disabled-bgc');
			$('.verify').removeClass('send-verify');

			$('.order').removeClass('orange');
			$('.order').removeClass('view-order');
		}
	},

	_sendVerify: function () {
		console.debug('phone', VerifyParams.phone, VerifyParams.code);
		ApiServer._buyTicketLogin(VerifyParams.phone, VerifyParams.code).done(function (json) {
			// console.log(json);
			if (json._status != 0) {
				alert(json._detail);
				$('.err-verify').css('visibility', 'visible');
				$('#code').val('');
				$('.code i').css('display', 'none');
				$('.verify').addClass('disabled-bgc');
				$('.verify').removeClass('submit-bgc');
				$('.verify').removeClass('send-verify');

				$('.order').removeClass('orange');
				$('.order').removeClass('view-order');
				return;
			};

			var openid = Common._getUrlParams();
			var localData = {
				'apikey': json.user.api_key,
				'openid': openid['openid'],
				'phone': json.user.phone,
				'username': json.user.username,
				'object_id': json.user.object_id
			};

			// alert(JSON.stringify(localData));

			Storage.setSessionStorage('user', localData);
			window.location.href = 'sections/activity-list/activity-list.html';
		});
	},

};