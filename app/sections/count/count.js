$(document).ready(function () {
	Count.getCountData();
});

(function ($) {
	getCountData = function () {
		ApiServer._getCountData().done(function (json) {
			// console.log(json);
			setTemplat(json);			
		})
	};

	setTemplat = function (json) {
		$('.count-total-num').text(json.all_count);
		$('.count-total-sale').text(json.all_sales/100);

		json.daily_sales.forEach(function (elem, item) {
			 setDayTpls(elem);
		})

	};

	setDayTpls = function (json) {
		var text = `
				<tr class="count-day-data">
					<td class="count-date">` + json.date + `</td>
					<td class="count-ticket-num">` + json.count + `</td>
					<td class="count-ticket-sale">` + json.sales/100 + `</td>
				</tr>
				`;

		$('.count-list').prepend(text);
		$('.count-day-data').insertAfter('.count-day-title');

	};

	window.Count = {
		getCountData : getCountData
	};
})(jQuery)