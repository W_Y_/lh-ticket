$(document).ready(function () {
	Success._getOrderInf();

	Storage.removeSessionStorage('orderData');

	$('.success-order').click(function () {
		window.location.href = '/ticket/sections/activity-list/activity-list.html';
	})
});

var Success = {
	_getOrderInf: function () {
		var orderId = Common._getUrlParams()['orderId'];
		// console.log(orderId);
		ApiServer._getOrderDetail(orderId).done(function (json) {
			// console.log(json);
			// alert(JSON.stringify(json))

			if (json.is_paid == false) {
				window.location.href = '/ticket/sections/home/home.html';
			}

			$('.qrcode img').attr('src', '' + json.profile_qr);
			$('.order-num span').text(json.order_no);

			for (var i = 0; i < json.details.length; i++) {
				Success._setOrderTicketList(json.details[i]);
			}
		});
	},

	_setOrderTicketList: function (json) {
		var text = '<p>' + json.ticket + '：' + json.count + '张（' + json.exchange_desc + '）</p>';

		$('.order-ticket-list').append(text);
	}
};