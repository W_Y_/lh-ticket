$('#alert').load('/ticket/template/alert.html');

$(document).ready(function () {
	Exchange._getOrderInf();

	Order_params.exchangeType = Common._getUrlParams()['type'];

	$('.exchange-button').click(Exchange._ticketExchange);

	$('.ticket-list').on('click', '.checkbox' , function (e) {
		Exchange.checkedTicket(e);
	});
});

var Order_params = {
	'order': '',
	'idList': [],
	'exchangeType': ''
}

var Exchange = {
	_getOrderInf : function () {
		var data = Storage.getSessionStorage('exchangeOrder');
		Order_params.order = data.object_id;
		for (var i = 0; i < data.details.length; i ++) {
			if (data.details[i].exchange_status == 1) {
				$('.ticket-empty').hide();
				Exchange._setTicketList(data.details[i]);
			}
		}

		// console.log(data.details)
	},

	_setTicketList : function (json) {
			
		var text = '<p><span class="checkbox" id="' + json.object_id + '"></span>' + json.ticket + '<span class="orange">' + json.count + '张</span></p>' 
		
		$('.ticket-list').append(text);
	},

	_ticketExchange : function () {
		if (!Alert) {
			return;
		};
			
		ApiServerInternal._exchangeTicket(Order_params.order, Order_params.idList, Order_params.exchangeType).done(function (json) {
			// console.log(json);

			if (json.code != 1) {
				alert(json.message);
				return;
			};
			
			Alert._setAlertText('兑换成功!');
			Alert._showAlert();
			
			setTimeout(function () {
				Storage.removeSessionStorage('exchnageOrder');
				window.location.href = '/ticket/sections/verify/verify-type.html';
			}, 2000);
		});
	},

	checkedTicket : function (e) {
		var target = $('#' + e.target.id);
		target.toggleClass('ticket-ckecked');

		if (target.hasClass('ticket-ckecked')) {
			Order_params.idList.push({
				object_id: target[0].id
			});
		} else {
			Order_params.idList = $.grep(Order_params.idList, function(value) {
			 return value.object_id != target[0].id;
			});
		};

		// console.log(Order_params.idList);
	},
}