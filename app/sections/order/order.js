$('#alert').load('/ticket/template/alert.html');
$(document).ready(function () {
	Order._mergeTickeetsData();
	$('.cancel-order').click(function () {
		window.location.href = "/ticket/sections/home/home.html";
	})

	$('.pay-order').click(function () {
		if (!Order_Params.status) return;
		Order._createOrder();
	})
})

var Order_Params = {
	'tickets': [],
	'exhibitionId': '',
	'status': true
}

var Order = {

	_setCoverImg :function (data) {
		SetExInf._checkExhibitionInformation(data);
	},

	_mergeTickeetsData : function () {
		var orderList = Storage.getSessionStorage('orderData')?Storage.getSessionStorage('orderData'):false;
		var exData = Storage.getLocalStorage('exInf')?Storage.getLocalStorage('exInf'):false;
		var tickets = exData.tickets;

		Order._setCoverImg(exData);
		Order_Params.exhibitionId = exData.object_id;

		if (!orderList || !exData) {
			window.location.href = '/ticket/sections/home/home.html';
		};

		orderList.forEach(function (elem, item) {
			tickets.forEach( function (telem, titem) {
				if (elem.ticket == telem.object_id) {
					Order_Params.tickets.push(Object.assign(telem,elem));
				}
			})
		})

		Order_Params.tickets.forEach(function (elem, item) { 
			Order._setOrderTicketListTpls(elem);
		})

		Order._sumOfMoney();
	},

	_setOrderTicketListTpls : function (order) {
		var text, tplA, tplB, tplC, tplD;

		tplA = '<img src="' + order.cover_image + '" alt="ticket">';

		tplB = '<p class="ticket-name">' + order.description + '</p>';

		tplC = '<p class="ticket-price">' + order.price / 100 + '元</p>';

		tplD = '<span class="ticket-num-count orange">' + order.count + '张</span>';

		text = `
			<div class="ticket-type" id="` + order.object_id + `">
				<div class="ticket-cover">
					`+ tplA +`
				</div>
				<div class="ticket-inf black">
					` + tplB + tplC +`
				</div>
				<div class="ticket-num">
					<span class="sign-num"></span>
					` + tplD + `
				</div>
			</div>
		`;

		$('.order-list').append(text);
	},

	_sumOfMoney : function () {
		var sum = 0;

		Order_Params.tickets.forEach(function (elem, item) {
			sum = sum + (elem.price * elem.count);
		})

		$('.total-count span').text(sum / 100);
	},

	_createOrder : function () {
		Order_Params.status = false;

		var items = {};
		Order_Params.tickets.forEach(function (elem, item) {
			items[elem.object_id] = elem.count;
		})

		ApiServer._createOrder(Order_Params.exhibitionId, items).done( function (json) {
			// console.log(json);
			// alert(JSON.stringify(json))
			if (!json.object_id) {
				alert(json.message);
				return;
			}
			
			Order._getPaymentConfig(json.object_id);
		}) ;
	
	},

	_getPaymentConfig : function (orderId) {
		var openid = Storage.getSessionStorage('user')['openid'];
		ApiServer._getWechatPaymentConfig(orderId, openid).done(function (json) {
			// alert(JSON.stringify(json))
			if (json.status == 3) {
       			window.location.href = "/ticket/sections/pay-success/pay-success.html?orderId=" + orderId
				return;
			}
			
			Order_Params.status = true;

			window.location.href = "http://crm.locationhunter.net/crm-wechat/wx_prepay/?type=ticket"
				+ "&orderid=" + orderId
				+ '&appId=' + json.appId
				+ '&timeStamp=' + json.timeStamp
				+ '&nonceStr=' + json.nonceStr
				+ '&package=' + json.package.replace(/=/, '%3d')
				+ '&signType=' + json.signType
				+ '&paySign=' + json.paySign;
		})
	},
};