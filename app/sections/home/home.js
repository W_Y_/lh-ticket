
$(document).ready(function () {
	Home._getUserInformation();
	 
	Params_Home.order = Storage.getSessionStorage('orderData')?Storage.getSessionStorage('orderData') : [];

	if (Params_Home.order.length > 0) {
		Status_Home.show_payment_button = true;
		Home._showPaymentButton();
	}

	$('.cancel-order').click(function () {
		window.location.href = '/ticket/sections/activity-list/activity-list.html';
	});

	$('footer').on('click', '.submit-bgc', function () {
		Storage.setSessionStorage('orderData', Params_Home.order);
		window.location.href = '/ticket/sections/order/order.html';
	});
});
var eventObjectList = []; //['ticket-type-a', 'ticket-type-b', 'ticket-type-c']
var Status_Home = {
	'show_payment_button' : false
};
var Params_Home= {
	'exData': '',
	'tickets': '',
	'order': [],
	'freeTickets':[]
};

var Home = {
	userInf : {},

	_setCoverImg :function (data) {
		SetExInf._checkExhibitionInformation(data);
	},

	_getUserInformation : function () {
			
		var userId = Storage.getSessionStorage('user')?Storage.getSessionStorage('user')['object_id'] : false;

		if (!userId) {
			alert('登录信息已失效，请重新登录');
			window.location.href = '/ticket/index.html';
			return;
		}

		ApiServer._getUserInf(userId).done(function (json) {
			Home.userInf = json;
			Home._getExhibitionId();
		}).fail(function (err) {
			console.log('获取用户信息失败');
		})
	},

	_getExhibitionId : function () {
		var activityId = Common._getUrlParams()['id'] ? Common._getUrlParams()['id'] : Storage.getSessionStorage('activityId');
		
		if (activityId == '' || activityId == undefined) {
			alert('请选择活动');
			window.location.href = "/ticket/sections/activity-list/activity-list.html";
		};

		Storage.setSessionStorage('activityId', activityId);

		Home._getExhibition(activityId);
		Home._getActivityDetail(activityId);
	},

	_getActivityDetail : function (exhibitionId) {
		ApiServer._getExhibitionDetail(exhibitionId).done(function (json) {
			// console.log(json);
			Storage.setLocalStorage('exInf', json);
			Home._setCoverImg(json);
			Home._setInformationTpls(); 
		});
	},

	_getExhibition : function (exhibitionId) {
		ApiServer._getExhibitionTicketList(exhibitionId).done(function (json) {
			// console.log(json);
			Params_Home.tickets = json.objects;
			var tickets = json.objects;
			var order = Params_Home.order;
			var havaVipFreeTicket = false;

			tickets.forEach(function (elem, item) {
				if (elem.ticket_type == 1 && elem.is_vip_member && elem.left_chance > 0) {
					havaVipFreeTicket = true;	
					Params_Home.freeTickets.push(elem.object_id);				
				}
			});

			if (order.length > 0) {
				var haveOrder = false;

				tickets.forEach(function (elem, item) {
					// if (elem.is_vip_member && elem.left_chance == 0 && elem.ticket_type == 1) {
					// 	console.log('vip客户免费活动本月已参加，下月可买免费票');
					// 	return;
					// }

					// if (!elem.is_vip_member && elem.ticket_type == 1) {
					// 	console.log('非vip用户不显示免费票');
					// 	return;
					// }

					// if (havaVipFreeTicket && elem.ticket_type == 2) {
					// 	console.log('vip客户免费活动本月还未参加');
					// 	return;
					// }

					Home._setTicketTpls(elem, 0);

					order.forEach( function (oelem, oitem) {
						if (elem.object_id == oelem.ticket) {
							Home._setTicketTpls(elem, oelem.count);
						}
					});

					// console.log(elem);
					eventObjectList.push(elem.object_id);
				})
			} else {

				tickets.forEach(function (elem, item) {
					// if (elem.is_vip_member && elem.left_chance == 0 && elem.ticket_type == 1) {
					// 	console.log('vip客户免费活动本月已参加，下月可买免费票');
					// 	return;
					// }

					// if (!elem.is_vip_member && elem.ticket_type == 1) {
					// 	console.log('非vip用户不显示免费票');
					// 	return;
					// }

					// if (havaVipFreeTicket && elem.ticket_type == 2) {
					// 	console.log('vip客户免费活动本月还未参加');
					// 	return;
					// }

					Home._setTicketTpls(elem, 0);
					// console.log(elem);
					eventObjectList.push(elem.object_id);
				});
			}
			Home._setWatchEvent();

		});
	},

	_setInformationTpls : function () {
		Params_Home.exData = Storage.getLocalStorage('exInf')?Storage.getLocalStorage('exInf'): false;
		var data = Params_Home.exData;
		if (!Params_Home.exData) {
			window.location.reload();
			return;
		};

		var text, tplA, tplB, tplC;

		tplA = data.address;

		tplB = Common.getNowFormatDate(data.start_time);
		tplC = Common.getNowFormatDate(data.end_time);

		text = `
			<p>` + tplA + `</p>
			<p>` + tplB.date + `-` + tplC.date + ` / ` + tplB.time + `-` + tplC.time + `</p>
		`;

		$('.ex-information').append(text);
	},

	_setTicketTpls : function (ticket, count) {	
		var text, tplA, tplB, tplC, tplE;

		tplA = '<img src="' + ticket.cover_image + '" alt="ticket">';
		
		tplB = '<p class="ticket-name">' + ticket.description + '</p>';
		
		tplC = '<p class="ticket-price">' + ticket.price / 100 + '元</p>';
		
		tplD = ''; //遮罩
		
		if ( ticket.count == ticket.sold_count ) {
			tplD = '<div class="ticket-empty"><div class="ticket-empty-inf">票已售完</div></div>'
		} else {
			
			if (ticket.is_vip_member && ticket.left_chance == 0 && ticket.ticket_type == 1) {
				console.log('vip客户免费活动本月已参加，下月可买免费票');
				tplD = '<div class="ticket-empty"><div class="ticket-empty-inf">您本月的免费次数已用完</div></div>'
			};

			if (!ticket.is_vip_member && ticket.ticket_type == 1) {
				console.log('非vip用户不显示免费票');
				tplD = '<div class="ticket-empty"><div class="ticket-empty-inf">您还不是vip用户或已过期<p><a href="http://crm.locationhunter.net/wechat_jump/">点击成为vip</a></p></div></div>'
			};
		};

		if (ticket.ticket_type == 1) {
			tplE = `<span class="ticket-num-min ticket-num-min-vip orange"></span>
					<div class="ticket-num-value">
					<span class="yellow-bgc">` + count + `</span>
				</div>
				<span class="ticket-num-max ticket-num-max-vip orange"></span>`;

		} else {
			tplE = `<span class="ticket-num-min orange"></span>
					<div class="ticket-num-value">
				<span class="yellow-bgc">` + count + `</span>
				</div>
				<span class="ticket-num-max orange"></span>`;
		};


		text = `
			<div class="ticket-type" id="` + ticket.object_id + `">
				<div class="ticket-cover">
					`+ tplA +`
				</div>
				<div class="ticket-inf black">
					` + tplB + tplC + `
				</div>
				<div class="ticket-num">
					` + tplE + `
				</div>
				` + tplD + `
			</div>
		`;

		$('.ticket-list-empty').hide(); 

		if (count > 0) {
			$('#' + ticket.object_id).replaceWith(text);
		} else {
			$('.ticket-list').append(text);
		}

		if (Params_Home.order != '' && ticket.ticket_type == 1 && count == 0) {
			$('#' + ticket.object_id).fadeOut();
		}

	},

	_setWatchEvent : function () {
		for (var i = 0; i < eventObjectList.length; i++) {
			Home._watchAdditionNum(eventObjectList[i]);
			Home._watchsubtractionNum(eventObjectList[i]);
		};
	},

	_watchAdditionNum : function (id) {
	
		$('#' + id).on('click', '.ticket-num-max', function (event) {
			Home._additionNum(event);
		});
	},

	_watchsubtractionNum : function (id) {
		$('#' + id).on('click', '.ticket-num-min', function (event) {
			Home._subtractionNum(event);
		});
	},

	_additionNum : function (event) {
		// console.log(event);

		var isVip = event.currentTarget.className.indexOf('ticket-num-max-vip')>=0;
		var selfId = event.delegateTarget.id;

		var self = $(event.target).siblings('.ticket-num-value').children('span');


		var value = Number(self.text());
		
		Home._vipTicketShow(selfId, value + 1);

		if(isVip && value >=1){
			return;
		}
		self.text(value + 1); 

		if ( Params_Home.order.length > 0 ) {
			for (var i = 0; i < Params_Home.order.length; i++ ) {
				// console.log(Params_Home.order[i].ticket)
				// console.log(selfId)
				if (Params_Home.order[i].ticket == selfId) {
					Params_Home.order[i].count = value + 1;
					// console.log(Params_Home.order)
					Home._judgeTicketNum();
					Home._showPaymentButton();
					return;
				}	
			};

			Params_Home.order.push({
				'ticket': selfId,
				'count': value + 1
			})
		} else {
			Params_Home.order.push({
				'ticket': selfId,
				'count': value + 1
			})
		};
		// console.log(Params_Home.order)
		Home._judgeTicketNum();
		Home._showPaymentButton();
	},

	_subtractionNum : function (event) {

		var selfId = event.delegateTarget.id;

		var self = $(event.target).siblings('.ticket-num-value').children('span');

		var value = Number(self.text());
		

		if (value == 0) {
			return;
		} 

		self.text(value - 1); 

		Home._vipTicketShow(selfId, value - 1);
		
		Params_Home.order.forEach(function (elem, item, arr) {
			if (elem.ticket == selfId) {
				elem.count = value - 1;
				if (elem.count == 0) {
					arr.splice(item,1);

				}
			}
		});

		// console.log(Params_Home.order);
		Home._judgeTicketNum();
		Home._showPaymentButton();
	},

	_vipTicketShow : function (id, value) {
		Params_Home.freeTickets.forEach(function (elem, item) {
			if (elem.object_id != id && elem.ticket_type == 1) {
				if (value > 0) {
					$('#' + elem.object_id).fadeOut();
				} else {
					$('#' + elem.object_id).fadeIn();
				}
			}
		})
	},

	//if number > 0 show payment button
	_judgeTicketNum : function () {
		if (Params_Home.order.length == 0) {
			Status_Home.show_payment_button = false;
			return;
		}

		for (var i = 0; i < Params_Home.order.length; i ++) {
			if (Params_Home.order[i].count > 0) {
				Status_Home.show_payment_button = true;
				return;
			}
		}
	},

	_showPaymentButton : function () {
		if (Status_Home.show_payment_button) {
			$('.go-payment').removeClass('disabled-bgc');
			$('.go-payment').addClass('submit-bgc');
		} else {
			$('.go-payment').removeClass('submit-bgc');
			$('.go-payment').addClass('disabled-bgc');
		}
	}
};