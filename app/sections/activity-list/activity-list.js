
$(document).ready(function () {
	Storage.removeSessionStorage('orderData');
});

(function ($) {
	var activityData;

	getActivityList = function () {
		ApiServer._getExhibitionList().done(function (json) {
			// console.log(json);
			activityData = json;
			json.objects.forEach(function (elem, item) {
				setActivityTel(elem);
			})
		});
	};

	setActivityTel = function (json) {
		var text, tplB, tplC,

		tplB = Common.getNowFormatDate(json.start_time);
		tplC = Common.getNowFormatDate(json.end_time);

		text = 	'<div class="activity-list-item">' +
				'<a href="/ticket/sections/home/home.html?id=' + json.object_id + '">' +
				'<div class="activity-item-cover" style="background: url(' + json.cover_image + '); no-repeat ; background-size: cover;background-position:center center;"></div>' +
				'<div class="activity-item-detail">' +
				'<p class="activity-detail-title">' + json.title  + '</p>' +
				'<div class="activity-detail-date">' + 
				'<p>' +  tplB.date + `-` + tplC.date + '</p>' + 
				'<p>' + tplB.time + `-` + tplC.time + '</p>' +
				'</div>' +
				'</div>' +
				'</a>' +
				'</div>';

		$('.activity-list').append(text);
	};

	window.Activity = {
		activityData : activityData
	}
	getActivityList();
})(jQuery)