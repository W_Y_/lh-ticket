$(document).ready(function () {
	UserOrder._getOrderList(3);

	$('footer').click(function () {
		window.location.href = '/ticket/sections/activity-list/activity-list.html';
	})

	$('.order-list').on('click','.order-detail',function (event) {
		// console.log($(event)[0].target.id);
		window.location.href = '/ticket/sections/pay-success/pay-success.html?orderId=' + $(event)[0].target.id;
	});


	$('.order-list').on('click','.order-pay',function (event) {
		UserOrder._getPaymentConfig($(event)[0].target.id);
	});

	$('.pay-status span').click(function (event) {
		var payStatus, payId;

		payId = $(event)[0].target.id;

		payId == 'pay-end' ? payStatus = 3 : payId == 'pay-pend' ? payStatus = 1 : payStatus = 4;

		UserOrder._changeStatusBtn(payId);

		UserOrder._getOrderList(payStatus);

	})
})

var User_Order_Params = {
	'orders': [],
	'exchangeDetail': []
}
 
var UserOrder = {
	_getOrderList : function (statu) {
		ApiServer._getOrder(statu).done(function (json) {
			$('.order-list').empty();
			$('.order-empty').show();

			// console.log(json);

			json.objects.forEach(function (elem, item) {
				UserOrder._payStatus(elem, statu);
			})
		})
	},

	_payStatus : function (json, statu) {
		if (json.status == statu) {
			$('.order-empty').hide();
			UserOrder._setOrderList(json);
		}
	},

	_setOrderList : function (json) {
		var text, tplA, tplB;

		tplA = '';

		json.details.forEach( function (elem, item) {
			tplA = tplA + '<p>' + elem.ticket + '：' + elem.count + '张（' + elem.exchange_desc + '）</p>'
		})

		var time = Common.getNowFormatDate(json.created_at);

		if (json.status == 3) {
			tplB = `<span class="order-detail yellow-bgc" id="` + json.object_id + `">详情</span>`;
		} 

		if (json.status == 4) {
			tplB = '';
		} 

		if (json.status == 1) {
			tplB = `<span class="order-pay yellow-bgc" id="` + json.object_id + `">支付</span>`;
		}
		// console.log(time)

		text = `
			<div class="order">
				<div class="order-cost">
					<p>` + json.total_price / 100 + `元</p>
					`
					+ tplB +
					`
				</div>
				<div class="order-type">
					` + tplA + `
				</div>
				<div class="order-time">` + time.date + ' ' + time.time + `</div>
			</div>
		`;

		$('.order-list').append(text);
	},

	_changeStatusBtn : function (text) {
		$('.pay-status span').removeClass('yellow-bgc');
		$('.pay-status span').addClass('disabled-bgc');

		$('.pay-status #' + text).removeClass('disabled-bgc');
		$('.pay-status #' + text).addClass('yellow-bgc');
	},

	_getPaymentConfig : function (orderId) {
		var openid = Storage.getSessionStorage('user')['openid'];
		ApiServer._getWechatPaymentConfig(orderId, openid).done(function (json) {
			// alert(JSON.stringify(json))
			
			window.location.href = "http://crm.locationhunter.net/crm-wechat/wx_prepay/?type=ticket"
				+ "&orderid=" + orderId
				+ '&appId=' + json.appId
				+ '&timeStamp=' + json.timeStamp
				+ '&nonceStr=' + json.nonceStr
				+ '&package=' + json.package.replace(/=/, '%3d')
				+ '&signType=' + json.signType
				+ '&paySign=' + json.paySign;
		})
	},

};