$(document).ready(function () {
	Insider._getExhibitionList();

	Insider._watchPhoneNumber();
	Insider._watchCodeNumer();

	$('#code').val(''); // default code value
	$('#phone').val('');// default phone value

	$('.code').on('click', '.get-code', function () {
		Insider._getCode();
	});

	$('.code i').on('click', function () {
		$('#code').val('');
		$('.code i').css('display', 'none');
		VerifyParams.code = '';
		Insider._startVerify();
	});

	$('footer').on('click', '.send-verify', function () {
		Insider._sendVerify();
	})
});

var VerifyParams = {
	'phone' : '',
	'code' : ''
}; 

var Status = {
	'countdown' : 'false',
};


var Insider = { 
	_getExhibitionList : function () {
		ApiServer._getExhibitionList().done(function (json) {
			// console.log(json);
			Insider._getExhibition(json.objects[0].object_id);			
		});
	},

	_getExhibition : function (exhibitionId) {
		ApiServer._getExhibitionDetail(exhibitionId).done(function (json) {
			// console.log(json);
			Storage.setLocalStorage('exInf', json);
			// Storage js object
			SetExInf._checkExhibitionInformation(json);

			var loginData = localStorage.getItem('internalUser')?localStorage.getItem('internalUser'): false;

			// console.log(loginData)
			if (loginData != '' && loginData != 'undefined' && loginData.apikey != '' && loginData.apikey != 'undefined') {
				window.location.href = '/ticket/sections/verify/verify-type.html';
			};
		});
	},

	_watchPhoneNumber : function () {
		var reg = /^1[0-9]{10}$/;
		var flag;
		$('#phone').on('keyup', function () {
			var numValue = $('#phone').val();
			flag = reg.test(numValue);
			if (numValue.length == 11 && Status.countdown == 'false' && flag == true) {
				VerifyParams.phone = numValue;
				console.log(VerifyParams);
				$('.code span').removeClass('disabled-bgc');
				$('.code span').addClass('submit-bgc');
				$('.code span').addClass('get-code');
			} else {
				VerifyParams.phone = '';
				$('.code span').removeClass('get-code');
				$('.code span').removeClass('submit-bgc');
				$('.code span').addClass('disabled-bgc');
			}
		})

	},

	_watchCodeNumer : function () {
		$('#code').on('keyup', function () {
			var codeValue= $('#code').val();
			if (codeValue.length > 0) {
				$('.code i').css('display', 'inline-block');
				VerifyParams.code = codeValue;

				Insider._startVerify();
			} else {
				$('.code i').css('display', 'none');
			}
		})
	},

	_getCode : function () { 
		ApiServer._sendMessage(VerifyParams.phone).done(function (json) {
			// console.log(json);
			Insider._countDown();
		});
	},

	_countDown : function () {
		Status.countdown = 'true';
		var time = 60;
		$('.code span').removeClass('get-code');
		$('.code span').removeClass('submit-bgc');
		$('.code span').addClass('disabled-bgc');

		var countdown = setInterval(function () {
			time = time - 1;
			$('.code span').text(time + 's')
		}, 1000);

		setTimeout(function () {	
			clearInterval(countdown);
			Status.countdown = 'false';
			$('.code span').text('获取');

			$('.code span').removeClass('disabled-bgc');
			$('.code span').addClass('submit-bgc');
			$('.code span').addClass('get-code');
		}, 60*1000)
	},

	_startVerify : function () {
		var result;

		if (VerifyParams.phone.length == 11 && VerifyParams.code.length > 0) {
			result = 'true';
		} else {
			result = 'false';
		}
		// console.log(result)
		if (result == 'true') {
			$('.verify').removeClass('disabled-bgc');
			$('.verify').addClass('submit-bgc');
			$('.verify').addClass('send-verify');
		} else {
			$('.verify').removeClass('submit-bgc');
			$('.verify').addClass('disabled-bgc');
			$('.verify').removeClass('send-verify');
		}
	},
	
	_sendVerify: function () {
		ApiServer._buyTicketLogin(VerifyParams.phone, VerifyParams.code).done(function (json) {
			// console.log(json);
			if (json._status != 0) {
				alert(json._detail);
				$('.err-verify').css('visibility', 'visible');
				$('#code').val('');
				$('.code i').css('display', 'none');
				$('.verify').addClass('disabled-bgc');
				$('.verify').removeClass('submit-bgc');
				$('.verify').removeClass('send-verify');
				return;
			};
			
			// 1 internal 2 external
			if (json.user.user_type != 1) {
				alert('请使用工作人员手机号码进行登录');
				return;
			};

			var openid = Common._getUrlParams();
			var localData = {
				'apikey': json.user.api_key,
				'openid': openid['openid'],
				'phone': json.user.phone,
				'username': json.user.username,
				'object_id': json.user.object_id,
				'usertype': json.user.user_type
			};

			// alert(JSON.stringify(localData));

			Storage.setLocalStorage('internalUser', localData);
			window.location.href = '/ticket/sections/verify/verify-type.html';
		});
	}
};

