$('#alert').load('/ticket/template/alert.html');
$(document).ready(function () {
	$('#phone-number').focus();
	VerifyPhone._watchPhoneNumber();

	$('.verify-phone-number').click(VerifyPhone._getOrderDetail);

	$('.order-list').on('click','.order-exchange',function (event) {
			
		for (var i = 0; i < VerifyPhone_params.length; i ++) {
			if (VerifyPhone_params[i].object_id ==  $(event)[0].target.id) {
				Storage.setSessionStorage('exchangeOrder', VerifyPhone_params[i]);
			}
		};

  		window.location.href = '/ticket/sections/exchange-ticket/exchange-ticket.html?type=phone';
	});

});
var VerifyPhone_params = [];
var Status_Phone = {
	'showSubmitButton': ''
};

var VerifyPhone = {
	_watchPhoneNumber : function () {
		$('#phone-number').keyup(function () {
			var value = $('#phone-number').val();

			// console.log(value);
			
			if (value.length < 11) {
				Status_Phone.showSubmitButton = false;
				VerifyPhone._showSubmitButton();
			}

			if (value.length > 10) {
				var num = value.slice(0, 11);
				$('#phone-number').blur();
				$('#phone-number').val(num);
				Status_Phone.showSubmitButton = true;
				VerifyPhone._showSubmitButton();
			}

			if (value.length == 11) {
				Status_Phone.showSubmitButton = true;
				VerifyPhone._showSubmitButton();
			}
		});

	},

	_showSubmitButton : function () {
		if (Status_Phone.showSubmitButton) {
			$('.verify-phone-number').removeClass('disabled-bgc');
			$('.verify-phone-number').addClass('submit-bgc');
		} else {
			$('.verify-phone-number').removeClass('submit-bgc');
			$('.verify-phone-number').addClass('disabled-bgc');
		}
	},

	_getOrderDetail : function () {
		var phone = $('#phone-number').val();
		ApiServerInternal._getOrderDetailPhone(phone).done(function (json) {
			// console.log(json)
			$('.order-list').empty();

			if (json.objects.length == 0) {
				Alert._setAlertText('未能获取订单信息！');
				Alert._showAlert();
				setTimeout(function () {
	              	 $('#phone-number').val('');
	              	Alert._cancelSHowAlert();
				}, 1000);
				return;
			};

			json.objects.forEach(function (elem, item) {
				if (elem.is_paid && (elem.exchange_status == 1 || elem.exchange_status == 3)) {
					VerifyPhone._setOrderList(elem);
					VerifyPhone_params.push(elem);
				}
			})

			if (VerifyPhone_params.length == 0) {
				Alert._setAlertText('未能获取订单信息！');
				Alert._showAlert();
				setTimeout(function () {
	              	 $('#phone-number').val('');
	              	Alert._cancelSHowAlert();
				}, 1000);
				return;
			};

			$('.order-list').show();

			// console.log(VerifyPhone_params);
		});
	},

	_setOrderList : function (json) {
		var text, tplA;

		tplA = '';

		for (var i = 0 ; i < json.details.length; i++) {
			tplA = tplA + '<p>' + json.details[i].ticket + '：' + json.details[i].count + '张（' + json.details[i].exchange_desc + '）</p>'
		}

		var time = Common.getNowFormatDate(json.created_at);

		// console.log(time)

		text = `
			<div class="order">
				<div class="order-cost">
					<p>` + json.total_price / 100 + `元</p>
					<span class="order-exchange orange-bgc" id="` + json.object_id + `">验票</span>
				</div>
				<div class="order-type">
					` + tplA + `
				</div>
				<div class="order-time">` + time.date + ' ' + time.time + `</div>
			</div>
		`;

		$('.order-list').append(text);
	}
};