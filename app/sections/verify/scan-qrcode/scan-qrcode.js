$('#alert').load('/ticket/template/alert.html');
$(document).ready(function () {
	Scan._getConfigWechat();
});

var Scan = {
	_getConfigWechat : function () {
		var url = window.location.href;
		// console.log(url);
		WechatJsSdk._getConfigParams(url).done(function (json) {
			// console.log(json);
			var configParams = {
				'appId': json.appId,
				'timeStamp': json.timestamp,
				'nonceStr': json.nonceStr,
				'signature': json.signature
			}
			WechatJsSdk._readyConfig(configParams);
			Scan._wxReadyScan();
		});
	}, 

	_wxReadyScan : function () {
		wx.ready(function () {
      		wx.scanQRCode({
	            needResult: 1,
	            scanType: ["qrCode"], 
	            success: function (res) {
	            	if (res.errMsg == 'scanQRCode:ok') {
	            		Scan._getOrderInf(res.resultStr);
	            	} else {
	            		alert('请扫描正确的二维码');
		              	Scan._wxReadyScan();
	            	}
	            },

	            cancel: function (res) {
	              	window.location.href = '/ticket/sections/verify/verify-type.html';
	            },

	            fail:function(res){
	              	alert('扫描失败，请重试');
	              	window.location.href = '/ticket/sections/verify/verify-type.html';
	            }
	        });
        });
	},

	_getOrderInf : function (orderUrl) {
		ApiServerInternal._getOrderDetailWx(orderUrl).done(function (json) {
			if (!json.object_id) {
				// console.log(json.messgae);

              	Alert._setAlertText('未能获取订单信息！');
				Alert._showAlert();

				setTimeout(function () {
	              	Alert._cancelSHowAlert();
	              	Scan._wxReadyScan(); 
				}, 1000);
				
				return;
			};
				
			Storage.setSessionStorage('exchangeOrder', json);
			window.location.href = '/ticket/sections/exchange-ticket/exchange-ticket.html?type=qr';

		});
	},

}