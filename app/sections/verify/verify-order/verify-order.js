$('#alert').load('/ticket/template/alert.html');

$(document).ready(function () {
	$('#order-number').focus();
	VerifyOrder._watchOrderNumber();

	$('.verify-order-number').click(VerifyOrder._getOrderDetail);
});

var Status_Order = {
	'showSubmitButton': ''
};

var VerifyOrder = {
	_watchOrderNumber : function () {
		$('#order-number').keyup(function () {
			var value = $('#order-number').val();

			// console.log(value)
			
			if (value.length < 9) {
				Status_Order.showSubmitButton = false;
				VerifyOrder._showSubmitButton();
			}

			if (value.length > 8) {
				var num = value.slice(0, 9);
				$('#order-number').blur();
				$('#order-number').val(num);
				
				Status_Order.showSubmitButton = true;
				VerifyOrder._showSubmitButton();
			}

			if (value.length == 9) {
				Status_Order.showSubmitButton = true;
				VerifyOrder._showSubmitButton();
			}
		});
	},

	_showSubmitButton : function () {
		if (Status_Order.showSubmitButton) {
			$('.verify-order-number').removeClass('disabled-bgc');
			$('.verify-order-number').addClass('submit-bgc');
		} else {
			$('.verify-order-number').removeClass('submit-bgc');
			$('.verify-order-number').addClass('disabled-bgc');
		}
	},

	_getOrderDetail : function () {
		var order_no = $('#order-number').val();
		ApiServerInternal._getOrderDetailOrderNo(order_no).done(function (json) {
			// console.log(json);

			Storage.setSessionStorage('exchangeOrder', json);
			
			if (json.object_id && (json.exchange_status == 1 || json.exchange_status == 3)) {
          		window.location.href = '/ticket/sections/exchange-ticket/exchange-ticket.html?type=order';
			} else {
				// console.log(json.message);
				Alert._setAlertText('未能获取订单信息！');
				Alert._showAlert();
				setTimeout(function () {
	              	$('#order-number').val('');
	              	Alert._cancelSHowAlert();
				}, 1000);
			}
		}).fail(function (jqXHR, textStatus, err) {
			if (err != '') {
				Alert._setAlertText('未能获取订单信息！');
				Alert._showAlert();
				setTimeout(function () {
	              	$('#order-number').val('');
	              	Alert._cancelSHowAlert();
				}, 1000);
			}
		});
	}
};