
var gulp = require('gulp');

// Test server
var connect = require('gulp-connect');

var appFolder = './app/';

gulp.task('connectDist', function () {
    connect.server({
        root: appFolder + "/",
        port: 9999
    });
});

// Default task
gulp.task('default', ['connectDist']);
